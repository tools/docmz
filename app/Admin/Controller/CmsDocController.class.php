<?php

namespace Admin\Controller;

class CmsDocController extends CmsController
{
    // 导出菜单
    // cmslist、cmshandle为必须要到导出的字段
    static $export_menu = array(
        'content' => array(
            '用户文档' => array(
                'cmslist' => array(
                    'title' => '文档简要',
                    'hiddens' => array(
                        'cmshandle' => '文档简要管理'
                    )
                )
            )
        )
    );
    // 标识字段，该字段为自增长字段
    public $cms_pk = 'id';
    // 数据表名称
    public $cms_table = 'cms_doc';
    // 数据库引擎
    public $cms_db_engine = 'MyISAM';
    // 列表列出的列出字段
    public $cms_fields_list = array(
        'id',
        'uid',
        'title',
    );
    // 添加字段，留空表示所有字节均为添加项
    public $cms_fields_add = array();
    // 编辑字段，留空表示所有字节均为编辑项
    public $cms_fields_edit = array();
    // 搜索字段，表示列表搜索字段
    public $cms_fields_search = array();
    // 数据表字段
    public $cms_fields = array(

        'out_id' => array(
            'title' => '外部ID',
            'description' => '',
            'type' => 'text',
            'length' => 50,
            'default' => '',
            'rules' => 'required|searchable'
        ),

        'uid' => array(
            'title' => '用户ID',
            'description' => '',
            'type' => 'member_uid',
            'default' => '0',
            'rules' => 'required|searchable'
        ),

        'cat_id' => array(
            'title' => '分类ID',
            'description' => '',
            'type' => 'number',
            'default' => '0',
            'rules' => 'searchable|required|unsigned'
        ),

        'add_time' => array(
            'title' => '添加时间',
            'description' => '',
            'type' => 'datetime',
            'default' => '0',
            'rules' => 'searchable'
        ),

        'update_time' => array(
            'title' => '更新时间',
            'description' => '',
            'type' => 'datetime',
            'default' => '0',
            'rules' => 'searchable'
        ),

        'title' => array(
            'title' => '标题',
            'description' => '',
            'type' => 'text',
            'length' => 200,
            'default' => '',
            'rules' => ''
        ),

        'summary' => array(
            'title' => '摘要',
            'description' => '',
            'type' => 'text',
            'length' => 200,
            'default' => '',
            'rules' => ''
        ),

    );
}
