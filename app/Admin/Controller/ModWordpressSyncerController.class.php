<?php

namespace Admin\Controller;

use Home\Service\WordpressSyncerService;
use Think\Db;

class ModWordpressSyncerController extends ModController
{
    static $export_menu = array(
        'system' => array(
            '系统设置' => array(
                'config' => array(
                    'title' => 'Wordpress互联',
                    'hiddens' => array()
                ),
            )
        )
    );

    public function build($yummy = false)
    {
        parent::build($yummy);

        switch ($this->build_db_type) {
            case 'mysql' :

                $sqls = array();

                if (!$this->build_table_exists("wordpress_syncer_user_map")) {
                    $table_name = $this->build_db_prefix . "wordpress_syncer_user_map";
                    $sqls [] = "DROP TABLE IF EXISTS $table_name";
                    $sqls [] = "
								CREATE TABLE $table_name (

									id INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增ID',
									wp_id INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Wordpress中UserId',
									uid INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '系统UID',

									PRIMARY KEY (id),
									KEY(wp_id),
									KEY(uid)

								) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
								";
                }

                if (!$this->build_table_exists("wordpress_syncer_doc_map")) {
                    $table_name = $this->build_db_prefix . "wordpress_syncer_doc_map";
                    $sqls [] = "DROP TABLE IF EXISTS $table_name";
                    $sqls [] = "
								CREATE TABLE $table_name (

									id INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增ID',
									wp_id INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Wordpress中文档Id',
									doc_id INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '系统文档ID',

									PRIMARY KEY (id),
									KEY(wp_id),
									KEY(doc_id)

								) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
								";
                }

                break;
        }

        foreach ($sqls as $sql) {
            $this->build_db->execute($sql);
        }

        parent::build_success($yummy);
    }

    public function config()
    {
        $keys = array(
            'wordpress_syncer_enable',
            'wordpress_syncer_db_host',
            'wordpress_syncer_db_user',
            'wordpress_syncer_db_pwd',
            'wordpress_syncer_db_name',
            'wordpress_syncer_db_prefix',
            'wordpress_syncer_preview_url'
        );
        if (IS_POST) {
            $data = array();
            foreach ($keys as &$k) {
                tpx_config($k, I('post.' . $k, '', 'trim'));
            }
            // 测试能否链接成功

            if (tpx_config_get('wordpress_syncer_enable')) {

                $host = tpx_config_get('wordpress_syncer_db_host');
                $user = tpx_config_get('wordpress_syncer_db_user');
                $pwd = tpx_config_get('wordpress_syncer_db_pwd');
                $name = tpx_config_get('wordpress_syncer_db_name');
                $prefix = tpx_config_get('wordpress_syncer_db_prefix');

                $conn = mysql_connect($host, $user, $pwd);
                if ($conn) {
                    if (@mysql_select_db($name)) {
                        if (mysql_query("SELECT 1 FROM ${prefix}users")) {
                            $this->success('保存成功，已检测到Wordpress！');
                        } else {
                            $this->success('未检测到Wordpress数据表！');
                        }
                    } else {
                        $this->error('服务器连接成功，数据库 ' . $name . ' 不存在!');
                    }
                } else {
                    $this->error('连接数据库服务器 ' . $host . ' 失败!');
                }
            }

            $this->success('保存成功');
        }

        foreach ($keys as &$k) {
            $this->$k = tpx_config_get($k);
        }

        $this->display();
    }

}